// Palindrome Detector - Licensed under the MIT License
#include <iostream>
using namespace std;
string reverseString(string& inputstr) {
    string newinput(inputstr);
    reverse(newinput.begin(), newinput.end());
    return newinput;
}
int detector() {
    string input;
    cout << "Type the word you want to check:\n";
    cin >> input;
    // std::cout << input;
    if (reverseString(input) == input) {
        cout << input << " is a palindrome.";
    }

}
int main() {
    detector();
    return 0;
}
